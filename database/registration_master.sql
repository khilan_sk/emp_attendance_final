-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2021 at 11:52 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration_master`
--

CREATE TABLE `registration_master` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('0','1') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `address` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mobile_no` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `confirm_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `role` int(11) NOT NULL DEFAULT 1 COMMENT '0-admin,1-employee',
  `image` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `joining_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_master`
--

INSERT INTO `registration_master` (`id`, `first_name`, `last_name`, `date_of_birth`, `gender`, `address`, `mobile_no`, `email`, `password`, `confirm_password`, `role`, `image`, `joining_date`, `status`, `created_at`, `updated_at`) VALUES
(19, 'test', 'admin', '2021-01-11', '0', 'surat', '897465123', 'admin@admin.com', '123', '123', 0, 'cubes_colorful_flight_82298_1920x1080.jpg', '2021-01-18', 1, '2021-01-29 10:39:05', '2021-01-20 18:28:51'),
(20, 'employee', 'one', '2004-08-30', '1', 'Surat', '8456123', 'emp1@emp.com', '123', '123', 1, '3d_balls_rendering_lines_105159_1920x1080.jpg', '2015-08-28', 1, '2021-01-29 10:41:39', '2021-01-23 22:24:12'),
(21, 'emp', 'two', '2004-01-02', '1', 'surat', '546123', 'emp2@emp.com', '555', '555', 1, '2014_maserati_granturismo_mc_stradale_99954_3840x2160.jpg', '2021-01-01', 1, '2021-01-29 09:18:24', '2021-01-23 22:25:06'),
(27, 'oneemp', 'emp', '2005-03-17', '0', 'surat', '741852963', 'oneemp1@emp.com', '123', '123', 1, 'download.png', '2012-08-27', 1, '2021-01-29 04:09:00', '2021-01-28 04:59:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration_master`
--
ALTER TABLE `registration_master`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration_master`
--
ALTER TABLE `registration_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
